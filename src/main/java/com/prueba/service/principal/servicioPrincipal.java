package com.prueba.service.principal;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.prueba.service.entity.*;
import com.prueba.service.entity.verificaUserRequest;
import com.prueba.service.interfaz.obtieneEmpleados;
import com.prueba.service.interfaz.verificaUserJPA;

@Service
public class servicioPrincipal {

	@Autowired
	verificaUserJPA jpa;
	
	@Autowired
	obtieneEmpleados obtieneEm;
	
	public verificaUserRequest verificaUser(String verifica ) {
		String [] lista = verifica.split("-");
		verificaUserRequest result = new verificaUserRequest();
		try {
			
			result =jpa.verificaUser(lista[0]);	
			
			if(result.getContrasena().equals(lista[1])) {
				return result;

			}
			
		} catch (Exception e) {
e.getMessage();		}
		return new verificaUserRequest();
	}
	
	
	public String eliminarEmpleado(String cedula) {
		Integer ob = obtieneEm.obtieneEmpleado(cedula);
		empleados e = new empleados();
		e.setId_empleado(ob);
		jpa.eliminarRegistro(e);
		obtieneEm.eliminarRegistro(cedula);
		return "{\"hecho\":\"ok\"}";
		
	}
		
	public boolean validar(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}

	
	public boolean validarEmail(String email) {
		 Pattern pattern = Pattern
	                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	 
	        Matcher mather = pattern.matcher(email);
	 
	        if (mather.find() == true) {
	          return true;
	        } 
	        return false;
	        
	}
	
	public String guardaEmpleado(empleados verifica ) {
		verificaUserRequest ususario = new verificaUserRequest();
		String cedula=verifica.getCedula();
		if(verifica.getCedula().toString().length()<10) {
			cedula=0+verifica.getCedula().toString();
		}
		System.out.println(verifica.getCedula());
		if(cedula.length()!=10) {
			return "{\"error\":\"cedula\"}";
		}
		
		if( !this.validar(cedula)) {
			return "{\"error\":\"numero\"}";

			
		}	
		if(!validarEmail(verifica.getCorreo())) {
			return "{\"error\":\"email\"}";

			

		}
		
		
		
		verifica= obtieneEm.save(verifica);
		ususario.setId_empleado(verifica);
		ususario.setUsuario(String.valueOf(verifica.getCedula()));
		ususario.setEstado("A");
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		ususario.setFecha_registro(formato.format(new Date()));
		ususario.setContrasena(String.valueOf(verifica.getCedula()));	
		jpa.save(ususario);
		return "{\"error\":\""+verifica.getCedula().toString()+"\"}";
	
		
	}
	
	public List<empleados> obtieneEMPLEADOS(){
		List<empleados> empleados = new ArrayList<empleados>();
		empleados= obtieneEm.findAll();
		
		return empleados;
	}
	
	public List<empleados> filtrosDeBusqueda(verificaUser filtros){
		String [] filtro = filtros.getUser(). split("-");
		List<empleados> empleados = new ArrayList<empleados>();
		String busqueda = filtro[0];
		try {
			
			if(filtro[1].equals("vacunacion")) {
				empleados= obtieneEm.filtrosVacunado(busqueda);	
			} else  {
				empleados= obtieneEm.filtrosTipoVacuna(busqueda);	

			}
		} catch (Exception e) {
e.getMessage();
}
		
	
		
		return empleados;
	}
	
	public empleados obtenEmpleadoxcedul(verificaUser cedula) {
		empleados  empleado = new empleados();
		empleado = obtieneEm.obtenEmpleadoxcedeula(cedula.getUser());
		return empleado;
	}
	
	
	public String ActualizaEmpleados(empleados empleados) {
		String respuesta = "ok";
		try {
		obtieneEm.save(empleados);									
		} catch (Exception e) {
			return "error";
			}
		return respuesta;
}
}
