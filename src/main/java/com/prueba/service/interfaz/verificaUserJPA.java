package com.prueba.service.interfaz;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import com.prueba.service.entity.empleados;
import com.prueba.service.entity.verificaUserRequest;

@EnableJpaRepositories
public interface verificaUserJPA extends JpaRepository<verificaUserRequest, Integer> {

	
	@Query("select c from verificaUserRequest c where c.usuario = ?1 and c.estado='A'")
	public verificaUserRequest verificaUser(String usuario);
	
	
	@Transactional
	  @Modifying
	@Query("delete from verificaUserRequest where id_empleado=?1 ")
	 void  eliminarRegistro(empleados id_empleado);

}
