package com.prueba.service.interfaz;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import com.prueba.service.entity.empleados;

@EnableJpaRepositories
public interface obtieneEmpleados extends JpaRepository<empleados, String> {

	@Transactional
	  @Modifying
	@Query("delete from empleados where cedula=:cedula ")
	 void eliminarRegistro(@Param("cedula") String cedula);
	
	
	@Query("select id_empleado from  empleados where cedula=:cedula ")
	public Integer obtieneEmpleado(@Param("cedula") String cedula);
	
	@Query("select c from  empleados c where cedula=:cedula ")
	public empleados obtenEmpleadoxcedeula(@Param("cedula")String cedula);
	
	@Query("select h from  empleados h where h.estado_vacunacion=:estado_vacunacion")
	public List<empleados> filtrosVacunado(@Param("estado_vacunacion")String estado_vacunacion);
	
	@Query("select h from  empleados h where tipo_vacuna=:vacuna")
	public List<empleados> filtrosTipoVacuna(@Param("vacuna")String vacuna);
}
