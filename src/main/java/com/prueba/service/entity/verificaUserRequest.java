package com.prueba.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "usuarios")
public class verificaUserRequest {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_usuario;
	 @OneToOne
	@JoinColumn(name = "id_empleado",updatable = false, nullable = false)	
	 private empleados id_empleado;
	@Column
	private String usuario;
	@Column
	private String contrasena;
	@Column
	private String fecha_registro;
	@Column
	private String estado;
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_ssuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public empleados getId_empleado() {
		return id_empleado;
	}
	public void setId_empleado(empleados id_empleado) {
		this.id_empleado = id_empleado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
