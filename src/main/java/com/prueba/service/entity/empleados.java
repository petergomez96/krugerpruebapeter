package com.prueba.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "empleados")
public class empleados {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_empleado;
    @OneToOne
    @JoinColumn(name = "id_rol", updatable = false, nullable = false)
	private rol id_rol;
	@Column
	private String nombre;
	@Column
	private String apellido;
	@Column
	private String correo;
	@Column
	private String cedula;
	@Column
	private String fecha_nacimiento;
	@Column
	private String direccion;
	@Column
	private String telefono;
	@Column
	private String estado_vacunacion;
	@Column
	private String tipo_vacuna;
	@Column
	private String fecha_vacuna;
	@Column
	private String numero_dosis;
	public Integer getId_empleado() {
		return id_empleado;
	}
	public void setId_empleado(Integer id_empleado) {
		this.id_empleado = id_empleado;
	}
	public rol getId_rol() {
		return id_rol;
	}
	public void setId_rol(rol id_rol) {
		this.id_rol = id_rol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}
	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstado_vacunacion() {
		return estado_vacunacion;
	}
	public void setEstado_vacunacion(String estado_vacunacion) {
		this.estado_vacunacion = estado_vacunacion;
	}
	public String getTipo_vacuna() {
		return tipo_vacuna;
	}
	public void setTipo_vacuna(String tipo_vacuna) {
		this.tipo_vacuna = tipo_vacuna;
	}
	public String getFecha_vacuna() {
		return fecha_vacuna;
	}
	public void setFecha_vacuna(String fecha_vacuna) {
		this.fecha_vacuna = fecha_vacuna;
	}
	public String getNumero_dosis() {
		return numero_dosis;
	}
	public void setNumero_dosis(String numero_dosis) {
		this.numero_dosis = numero_dosis;
	}
	public empleados() {
		super();
	}
	
}
