package com.prueba.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.service.entity.empleados;
import com.prueba.service.entity.verificaUser;
import com.prueba.service.entity.verificaUserRequest;
import com.prueba.service.principal.servicioPrincipal;

@RestController
@RequestMapping(value="/pruebaKruger")
@CrossOrigin(exposedHeaders="Access-Control-Allow-Origin")

public class controller {
	
	
	@Autowired
	servicioPrincipal service;
	
	
	@PostMapping(value="/verificaUser")
	public ResponseEntity<?> verificaUsuario(@RequestBody @Validated String verifica){
		return  ResponseEntity.ok(service.verificaUser(verifica));
		
	}
	
	@PostMapping(value="/agregarEmpleados")
	public ResponseEntity<?> agregarEmpleado(@RequestBody @Validated empleados verifica){
	    return new ResponseEntity<String>(service.guardaEmpleado(verifica), HttpStatus.OK);

		
	}
	
	@PostMapping(value="/eliminarEmpleados")
	public ResponseEntity<?> eliminarEmleados(@RequestBody @Validated String cedula){

		return  ResponseEntity.ok(service.eliminarEmpleado(cedula));	
	}
	
	@GetMapping(value= "empleadoxcedula")
	public ResponseEntity<empleados> obtenEmpleadoxcedul( verificaUser cedula){
		return ResponseEntity.ok(service.obtenEmpleadoxcedul(cedula)); 
	}
	
	
	@GetMapping(value= "obtieneEmpleados")
	public ResponseEntity<List<empleados>> obtieneEmpleados(){
		return ResponseEntity.ok(service.obtieneEMPLEADOS());
	}
	

	@PostMapping(value= "/actualizaempleado")
	public ResponseEntity<?> ActualizarEmpleado(@RequestBody @Validated empleados empleados){
		return ResponseEntity.ok(service.ActualizaEmpleados(empleados));
	}
	

	@GetMapping(value= "filtrodebusqueda")
	public ResponseEntity<List<empleados>> filtrosdebusqueda(verificaUser busqueda){
		return ResponseEntity.ok(service.filtrosDeBusqueda(busqueda));
	}
	
}
